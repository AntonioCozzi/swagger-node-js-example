# Project Title

swagger-nodejs-example

## Getting Started

This project is a 101 about setting up swagger for a Node.js project.

### Installing

To run this project run the following commands:

```
$ npm i
$ npm run serve
```