const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const definition = {
    info: {
        title: "Hello World APIs",
        version: "1.0.0",
        description: "A documentation for Hello World project"
    },
    host: "localhost:8000",
    basePath: "/"
}

const options = {
    definition,
    apis: ['index.js']
}

const swaggerSpec = swaggerJSDoc(options)

const port = 8000;

/**
 * @swagger
 * /helloworld?{name}&{surname}:
 *  get:
 *      description: produces response for helloworld apis
 *      parameters:
 *          - name: name
 *            in: path
 *            description: Name of the user.
 *            schema:
 *                  type : string
 *          - name: surname
 *            in: path
 *            description: Surname of the user.
 *            schema:
 *                  type : string
 *      responses:
 *          200:
 *              description: A greeting using parameters "name" and "surname".
 *          500:
 *              description: An error.
 */
app.get("/helloworld", (req, res) => {
    const name = req.query.name;
    const surname = req.query.surname;

    if (name === undefined || surname === undefined) {
        res.json({
            error: `Invalid params.`
        });
        res.status(500)
    } else {
        res.json({
            message: `Hello World ${name} ${surname}!`,
            name,
            surname
        });
        res.status(200)
    }
})

app.get("/swagger.json", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
})

app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerSpec))
app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true,
}));
app.use(bodyParser.json());

app.listen(port, () => console.log(`App running on port ${port}`))